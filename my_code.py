def preaty_print(name):
    print("-" * 50)
    print(f"Hello, {name}!")
    print("-"*50)


def pretty_pretty_print(name):
    print(f"You are preaty {name}")
    print("*" * 50)


if __name__ == "__main__":
    name = "Vlad"
    preaty_print(name)
    pretty_pretty_print(name)
